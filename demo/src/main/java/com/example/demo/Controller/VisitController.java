package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Visit;
import com.example.demo.Service.VisitService;

@RestController
public class VisitController {

    @Autowired
    private VisitService visitService ;

    @GetMapping("/visits")
    public ArrayList<Visit> getAllVisit(){

        ArrayList<Visit> allVisit = visitService.getVisitList();
        return allVisit ;
    }

}
