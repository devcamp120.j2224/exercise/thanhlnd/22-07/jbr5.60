package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Customer;

@Service
public class CustomerService {

    Customer customer1 = new Customer("Lê Thạnh");
    Customer customer2 = new Customer("Châu Giang");
    Customer customer3 = new Customer("Trường Giang");

    public ArrayList<Customer> getCustomerList(){

        ArrayList<Customer> CustomerList = new ArrayList<>();

        CustomerList.add(customer1);
        CustomerList.add(customer2);
        CustomerList.add(customer3);

        return CustomerList ;
    }

}
