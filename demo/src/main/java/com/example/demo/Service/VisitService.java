package com.example.demo.Service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Visit;
@Service
public class VisitService {
    @Autowired
    private CustomerService customerService ;
    Visit visit1 = new Visit("Phú Quốc" , new Date());
    Visit visit2 = new Visit("Nha Trang", new Date());
    Visit visit3 = new Visit("Phan Thiết" , new Date());

    public ArrayList<Visit> getVisitList(){

        ArrayList<Visit> allVisit = new ArrayList<>();

        visit1.setCustomer(customerService.customer1);
        visit2.setCustomer(customerService.customer2);
        visit3.setCustomer(customerService.customer3);

        allVisit.add(visit1);
        allVisit.add(visit2);
        allVisit.add(visit3);

        return allVisit ;
    }
}
